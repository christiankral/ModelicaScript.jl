#!/usr/bin/julia --startup-file=no
#
# This Julia script is open source under the BSD 3-clause license
# https://gitlab.com/christiankral/ModelicaScript.jl
# Check README.md and LICENSE.md
#
# 0.4.0 2021-01-26
#       Create error message if row indexes are out of bound
# 0.3.2 2021-01-24
#       Remove obsolete println
# 0.3.1 2021-01-24
#       Fix case #01 if no 5th argument is provided
# 0.3.0 2021-01-21
#       Option to select row when creating text file
# 0.2.0 2021-01-19
#       Improve error message in case number of rows of CSV files is not equal
# 0.1.3 2021-01-13
#       Catch simulation errors, e.g., cause by licensing problems
# 0.1.2 2021-01-13
#       Fix error when not performing the entire job: do not call simulation
# 0.1.1 2021-01-13
#       Add more command line documentation
# 0.1.0 2021-01-13
#       Initial version with Dymola environment variables

using Base, DelimitedFiles, Printf

println("ModelicaScript.jl 0.4.0 (C) Christian Kral 2021-01-26")
nARGS=size(ARGS,1)
if nARGS==0
    println("\nModelicaScript.jl header margin csvfile txtfile csvresfile txtresfile mobin env")
    println("    header      integer number of header rows (usually 1 or 2)")
    println("    margin      integer number of left margin columns (usually 0 or 1)")
    println("    csvfile     CSV file to be provided to be converted to txtfile")
    println("    txtfile     text file to processed by Modelica simulation")
    println("    csvresfile  result CSV file created from Modelica result files")
    println("    txtresfile  text file created by Modelica simulation")
    println("    mobin       binary file of Modelica simulation to executed")
    println("    env         optional environment variables required by the binary")
    println("                Modelica simulation; \$ ist not allowed, so only")
    println("                explicit absolute paths are allowed")
    println("")
    println("ModelicaScript.jl header margin csvfile txtfile")
    println("               Only creates the txtfile for the Modelica simulation")
    println("               from csvfile")
    println("")
    println("ModelicaScript.jl header margin csvfile txtfile row")
    println("               Only creates the txtfile for the Modelica simulation")
    println("               from of csvfile (line of csvfile = row - header)")
    println("")
    println("It is required that unused CSV data cell elements are filled with 0")
    println("")
    exit()
end

# Get the first four command line arguments
println()
if nARGS>=4
    header = Int(parse(Float64,ARGS[1]))
    margin = Int(parse(Float64,ARGS[2]))
    csvFile = ARGS[3]
    txtFile = ARGS[4]
else
    error("    All four arguments header margin csvfile txtfile must be specified")
end
if nARGS==5
    extract = Int(parse(Float64,ARGS[5]))
else
    extract = 1
end

# Get the commendline arguments 5-7
if nARGS>=7
    csvresFile = ARGS[5]
    txtresFile = ARGS[6]
    mobin = ARGS[7]
    # Make the entire job
    entirejob = true
elseif nARGS!=4 | nARGS!=5
    error("\nWrong number of arguments is specified")
else
    # Do only create input file for Modelica
    entirejob = false
end
# Get the optional environment variable file
if nARGS>=8
    envFile = ARGS[8]
end

println("Set header                    = ",header)
println("    margin                    = ",margin)
println("    CSV input file            = ",csvFile)
println("    Modelica text input file  = ",txtFile)
if nARGS>5
    println("    CSV result file           = ",csvresFile)
    println("    Modelica text result file = ",txtresFile)
    println("    Modelica binary file      = ", mobin)
end
if nARGS>=7
    println("    Environment spec file     = ",envFile)
end

# Read header, margin and data from CSV file
function csvRead(csvFile;header=2,margin=0)
    if !isfile(csvFile)
        error("    File "*csvFile*" does not exist")
    else
        println("    Reading file "*csvFile)
    end
    csvContent=readdlm(csvFile,'\t',skipstart=0,quotes=false)
    csvHeader=csvContent[1:header,:]
    csvMargin=csvContent[:,1:margin]
    csvData=csvClean(csvContent[header+1:end,margin+1:end])
    return csvHeader,csvMargin,csvData
end

# Remove non-numeric data from numeric data section
function csvClean(csvData)
    # Change data values to 0, if string is loaded
    for r=1:size(csvData,1)
        for c=1:size(csvData,2)
            if !isa(csvData[r,c],Number)
                csvData[r,c]=NaN
            end
        end
    end
    return csvData
end

# Determine row of longest entry
function validDataRow(csvIndividualData)
    nColumn=zeros(size(csvIndividualData,1))
    for r in 1:size(csvIndividualData,1)
        for c in 1:size(csvIndividualData,2)
            # Check all columns c of rows r if at least one of them is numeric
            if typeof(csvIndividualData[r,c])==Float64 ||
                typeof(csvIndividualData[r,c])==Int64
              # If there exists at least one columns with float entry in row r,
              # then store row r in rIndividual
              if !isnan(csvIndividualData[r,c])
                  # Consider only non-zero values
                  nColumn[r]=nColumn[r]+1
              end
            end
        end
    end
    # Determine row with maximum number of elements and the
    # maximum number of elements, maxRow
    maxRow,row=findmax(nColumn)
    # Determine number of rows which are equal to maxRow elements
    # If threre is more than one row, set valid=false, otherwise valid=true
    valid=sum(nColumn.==maxRow)==1
    return row,valid
end

# Write header, margin and data from CSV file
function csvWrite(csvFile,csvHeader,csvMargin,csvData;header=2,margin=2)
    rDataEnd=size(csvData,1)
    csvContent = cat(csvHeader,cat(csvMargin[header+1:rDataEnd+header,:],csvData,dims=2),dims=1)
    status = writedlm(csvFile,csvContent,'\t',quotes=false)
    return status
end

RED="\033[0;31m" # Red color
NC="\033[0m" # No color

# Spearate CSV file name into name and extension
csvFileNameExt=splitext(csvFile)
csvFileName=csvFileNameExt[1]
csvFileExt=csvFileNameExt[2]

if entirejob
    # Create file names of other files to be processed
    # Result file
    csvResultFileNameExt=splitext(csvresFile)
    csvResultFileName=csvResultFileNameExt[1]
    csvResultFileExt=csvResultFileNameExt[2]
end

# Read base CSV file
println("\n    Reading data")
csvHeader,csvMargin,csvData = csvRead(csvFile,header=header,margin=margin)

if entirejob
    # Read result CSV file
    csvResultHeader,csvResultMargin,csvResultData =
    csvRead(csvresFile,header=header,margin=margin)
end

# Determine number of rows and columns of CSV input file
rows = size(csvData,1)
cols = size(csvData,2)
if entirejob
    # Determine number of rows and columns of CSV result file
    rowsResult  = size(csvResultData,1)
    colsResult  = size(csvResultData,2)
    if rows != rowsResult
        error("\nNumber of rows of CSV file (",csvFile,": ",rows,") and\nCSV result file (",csvresFile,": ",rowsResult,") are not equal")
    end
end

# Set environment variables
if entirejob
    # Read file env
    henv = open(envFile, "r")
    envlines = readlines(henv)
    close(henv)
    println("    Set environment variables")
    for k in 1:length(envlines)
        env2 = split(envlines[k],"=")
        # Remove leading and trailing strings from variable name
        var = strip(env2[1])
        # Convert value to Float64
        val = strip(env2[2])
        ENV[var]=val
        println("        " * envlines[k])
    end
end

# Define range of evaluation depending on if the entire job is done or
# only the Modelica input file is created
if entirejob
    rrange = 1:rows
else
    rrange = extract
end

for r in rrange
    if r > rows
        error("\n    Number of rows out of bounds (" * string(r) * " > " * string(rows) *")")
    end
    # Log case
    print("    Creating Modelica case #" * @sprintf("%02i",r))
    # Write text file of row r
    h = open(txtFile, "w")
    for c in 1:cols
        # Write name = value
        write(h, csvHeader[1,c] * " = " * string(csvData[r,c]) * "\n")
    end
    close(h)

    if entirejob
        # Run Modelica binary
        print(" => running simulation")
        try
            run(pipeline(`$mobin`,stdout="./simulation.log"))
        catch
            error("\n    Running the simulation failed: This may be a licensing issue\n    in case you are using Dymola or other binaries of commercial simulators")
        end

        # Read Modelica result file
        print(" => processing result file")
        h = open(txtresFile, "r")
        res = readlines(h)
        close(h)
        for c in collect(1:length(res))
          # Split read line at =
          res2 = split(res[c],"=")
          # Remove leading and trailing strings from variable name
          var = strip(res2[1])
          # Convert value to Float64
          val = parse(Float64,res2[2])
          # Write result to csvResultData
          global found
          found = false
          for rres in 1:colsResult
            # Look for name in var in result header
            if csvResultHeader[1,rres] == var
                csvResultData[r,rres] = val
                found = true
            end
          end
          if !found
            println("    " * var * " not found in CSV result file")
          end
        end
    end
    println()
end

if entirejob
    # Write result CSV file
    println("    Writing result CSV file")
    csvWrite(csvresFile,csvResultHeader,csvResultMargin,csvResultData,header=header,margin=margin)
end
println("    Done\n")
